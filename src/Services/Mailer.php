<?php


namespace App\Services;


class Mailer
{
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendEmail(string $name, string $email, string $message){
        $content = (new \Swift_Message('Contact'))
            ->setFrom([$email => $name])
            ->setSender($email)
            ->setTo(['asad60khan@gmail.com'])
            ->setBody($message);

        $this->mailer->send($content);
    }
}