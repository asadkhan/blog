<?php


namespace App\Application\Sonata\UserBundle\Form;

use App\Application\Sonata\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'Nom *',
                'required' => true
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Prenom *',
                'required' => true
            ])
            ->add('pictureTmp', FileType::class, [
                'label' => 'Photo de profil',
                'mapped' => false,
                'required' => false,
                'data_class' => null
            ])
            ->add('dateOfBirth', BirthdayType::class, [
                'label' => 'Date de naissance',
            ])
            ->add('phone', TextType::class, [
                'label' => 'Téléphone',
                'data_class' => null,
                'required' => false
            ])
            ->add('job', TextType::class, [
                'label' => 'Fonction',
                'data_class' => null,
                'required' => false
            ])
            ->add('biography', TextareaType::class, [
                'label' => 'Biographie',
                'data_class' => null,
                'required' => false
            ])
            ->add('facebookName', TextType::class, [
                'label' => 'Nom facebook',
                'data_class' => null,
                'required' => false
            ])
            ->add('twitterName', TextType::class, [
                'label' => 'Nom twitter',
                'data_class' => null,
                'required' => false
            ])
            ->add('gplusName', TextType::class, [
                'label' => 'Nom Google+',
                'data_class' => null,
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}