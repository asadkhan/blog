<?php


namespace App\DataTransformer;


use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\HttpFoundation\File\File;

class ImageTransformer implements DataTransformerInterface
{
    private $entityManager;
    private $params;

    public function __construct(EntityManagerInterface $entityManager, ParameterBagInterface $params)
    {
        $this->entityManager = $entityManager;
        $this->params = $params;
    }

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param  string|null $image
     * @return File|null
     */
    public function transform($image)
    {
        if (null === $image) {
            return null;
        }

        if($image){
            $uploadDir = $this->params->get('uploads_dir_fullpath');
            $file = new File($uploadDir . '/articles/' . $image);
            return $file;
        }

        return null;
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  File|null $file
     * @return string|null
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($file)
    {
        if (!$file) {
            return null;
        }

        return $file;
    }
}