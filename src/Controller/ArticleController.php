<?php

namespace App\Controller;

use App\Application\Sonata\UserBundle\Entity\User;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use App\Form\CommentType;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class ArticleController extends AbstractController
{
    private $em;
    private $paginator;
    private const LIMIT_ARTICLES = 8;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/articles", name="article_list")
     */
    public function articleList(Request $request)
    {
        $repositoryCategory = $this->em->getRepository(Category::class);
        $repositoryArticle = $this->em->getRepository(Article::class);

        $page = $request->query->getInt('page', 1);
        $categoryName = $request->query->get('category');

        $category = ($categoryName) ?
                $repositoryCategory->findOneBy(array(
                'name' => $categoryName
            )) :
            null;

        $query = (!$category) ?
            $repositoryArticle->getArticles(null, null, true) :
            $repositoryArticle->getArticlesByCategory($category->getId());

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            self::LIMIT_ARTICLES
        );

        return $this->render('article/article_list.html.twig', array(
            'pagination' => $pagination,
            'page' => $page,
            'categoryName' => $categoryName,
            'limitArticles' => self::LIMIT_ARTICLES
        ));
    }

    /**
     * @Route("/articles/{id_article}", name="article_detail")
     */
    public function articleDetail($id_article, Request $request)
    {
        $articleRepository = $this->em->getRepository(Article::class);
        $article = $articleRepository->find($id_article);

        $commentRepository = $this->em->getRepository(Comment::class);
        $comments = $commentRepository->getCommentsByArticle($article->getId());
//        dump($comments);
//        die;

        $comment = new Comment();
        $commentForm = $this->createForm(CommentType::class, $comment);

        if(!$this->getUser()){
            $commentForm->add('email', TextType::class, [
                'label' => 'Identifiant (ou email)*',
                'mapped' => false,
            ]);

            $commentForm->add('password', PasswordType::class, [
                'label' => 'Mot de passe*',
                'mapped' => false
            ]);
        }

        $commentForm->handleRequest($request);

        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $success = true;
            $comment = $commentForm->getData();

            if(!$this->getUser()){
                $userEmail = $commentForm->get('email')->getData();
                $userPassword = $commentForm->get('password')->getData();

                if($userEmail && $userPassword){
                    $success = $this->login($userEmail, $userPassword, $request);
                }
            }

            if($success){
                $comment->setUser($this->getUser());
                $article->addComment($comment);

                $this->em->persist($comment);
                $this->em->persist($article);
                $this->em->flush();
            }

            return $this->redirectToRoute('article_detail', [
                'id_article' => $article->getId()
            ]);
        }

        return $this->render('article/article_detail.html.twig', array(
            'article' => $article,
            'comments' => $comments,
            'commentForm' => $commentForm->createView()
        ));
    }

    /**
     * @Route("/articles/category/{slug_category}", name="article_list_by_category")
     */
    public function articleListByCategory($slug_category)
    {
        return $this->render('article/article_list_by_category.html.twig');
    }

    private function login(string $auth, string $password, Request $request){
        $encoder = new BCryptPasswordEncoder(13);
        $userRepository = $this->em->getRepository(User::class);

        $user = $userRepository->findOneBy([
            'email' => $auth
        ]);

        if(!$user){
            $user = $userRepository->findOneBy([
                'username' => $auth
            ]);
        }

        if(!$user){
            return false;
        }

        $validPassword = $encoder->isPasswordValid(
            $user->getPassword(),
            $password,
            $user->getSalt()
        );

        if($validPassword){
            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));
            $event = new InteractiveLoginEvent($request, $token);
            $dispatcher = new EventDispatcher();
            $dispatcher->dispatch("security.interactive_login", $event);
        }else{
            return false;
        }

        return true;
    }
}
