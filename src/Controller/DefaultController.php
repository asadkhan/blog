<?php

namespace App\Controller;

use App\Application\Sonata\UserBundle\Entity\Picture;
use App\Application\Sonata\UserBundle\Entity\User;
use App\Application\Sonata\UserBundle\Form\UserType;
use App\Entity\Article;
use App\Services\FileUploader;
use App\Entity\Contact;
use App\Form\ContactType;
use App\Services\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    private $em;
    private $paginator;
    private $fileUploader;
    private $mailer;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator, FileUploader $fileUploader, Mailer $mailer)
    {
        $this->em = $em;
        $this->paginator = $paginator;
        $this->fileUploader = $fileUploader;
        $this->mailer = $mailer;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(Request $request)
    {
        $page = $request->query->getInt('page', 1);

        $repository = $this->em->getRepository(Article::class);
        $mainArticle = $repository->getMainArticle();
        $mainArticle = ($mainArticle) ? $mainArticle : null;

        $query = $repository->getArticles(null, $mainArticle, true);
        $limitArticles = 4;

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limitArticles
        );

        return $this->render('default/home.html.twig', array(
            'mainArticle' => $mainArticle[0],
            'pagination' => $pagination,
            'page' => $page,
            'limitArticles' => $limitArticles
        ));
    }


    /**
     * @Route("/profile", name="profile")
     */
    public function profile(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $picture = $user->getPicture();
            $pictureTmp = $form->get('pictureTmp')->getData();

            if($pictureTmp){
                if($picture && $picture->getName()){
                    $this->fileUploader->removeFile($picture->getName(), 'users');
                }

                $file = new File($pictureTmp);
                $uploadFileName = $this->fileUploader->uploadFile($file, 'users');

                if($uploadFileName){
                    //If superadmin and not have picture
                    if(!$picture){
                        $newPicture = new Picture();
                        $newPicture->setUser($user);

                        $this->em->persist($newPicture);
                        $this->em->persist($user);
                        $this->em->flush();

                        $newUser = $this->em->getRepository(User::class)->find($user->getId());
                        $picture = $newUser->getPicture();
                    };

                    $picture->setName($uploadFileName);
                    $picture->setFullPath($this->fileUploader->getUploadDirFullPath() . '/users/' . $uploadFileName);
                    $picture->setWebPath($this->fileUploader->getUploadDirWebPath() . '/users/' . $uploadFileName);
                }
            }

            $this->em->persist($user);
            $this->em->flush();
        }

        return $this->render('default/profile.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/about", name="about")
     */
    public function about()
    {
        return $this->render('default/about.html.twig');
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request)
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);

        $user = $this->getUser();
        $messages = [];

        if($user){
            $form->get('name')->setData($user->getFullName());
            $form->get('email')->setData($user->getEmail());
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact = $form->getData();
            $this->mailer->sendEmail($contact->getName(), $contact->getEmail(), $contact->getMessage());
            $messages[] = "L'email a bien été envoyé !";
        }

        return $this->render('default/contact.html.twig', [
            'contactForm' => $form->createView(),
            'messages' => $messages
        ]);
    }

    /**
     * @Route(name="footer_articles")
     */
    public function footerArticles(){
        $articleRepository = $this->em->getRepository(Article::class);

        $recentArticles = $articleRepository->getArticles(3);
        $recentArticlesByComment = $articleRepository->getLatestCommentedArticles(3);

        return $this->render('default/footer_articles.html.twig', [
            'recentArticles' => $recentArticles,
            'recentArticlesByComment' => $recentArticlesByComment
        ]);
    }
}
