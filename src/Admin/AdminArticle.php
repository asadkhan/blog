<?php


namespace App\Admin;

use App\Admin\Events\Form\AdminArticleFormListener;
use App\DataTransformer\ImageTransformer;
use App\Entity\ArticleTag;
use App\Entity\Category;
use App\Entity\Image;
use App\Services\FileUploader;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\Form\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Events\FormListener\AdminArticleListener;

class AdminArticle extends AbstractAdmin
{
    private $container;
    private $imageTransformer;
    private $fileUploader;

    public function __construct($code, $class, $baseControllerName, FileUploader $fileUploader, ImageTransformer $imageTransformer)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->fileUploader = $fileUploader;
        $this->imageTransformer = $imageTransformer;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $this->container = $this->getConfigurationPool()->getContainer();
        $article = $this->getSubject();

        $imageFormFieldsOptions = [
            'label' => 'Image',
            'mapped' => false,
            'required' => false,
            'data_class' => null
        ];

        if($image = $article->getImage()){
            $uploadDirectory = $this->fileUploader->getUploadDirWebPath() . '/articles';
            if($image->getName()){
                $uploadDirectory = $this->fileUploader->getUploadDirWebPath() . '/articles';
                $webPath = $uploadDirectory . '/' . $image->getName();
            }else{
                $imagesDirectory = $this->container->getParameter('images_dir_webpath');
                $defaultImage = $this->container->getParameter('article_default_picture');
                $webPath = $imagesDirectory . '/' . $defaultImage;
            }

            $imageFormFieldsOptions['help'] = '<img src="'.$webPath.'" class="admin-preview"/>';
        }

        $formMapper
            ->add('title', TextType::class, [
                'label' => 'Titre'
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Contenu'
            ])
            ->add('status', ChoiceType::class, [
                'label' => 'Publié',
                'choices'  => [
                    'Oui' => true,
                    'Non' => false,
                ],
            ])
            ->add('imageTmp', FileType::class, $imageFormFieldsOptions)
            ->add('imageSlug', TextType::class, [
                'label' => 'Image slug',
                'mapped' => false,
                'required' => false,
            ])
            ->add('isMainArticle', ChoiceType::class, [
                'label' => 'Article principale',
                'choices'  => [
                    'Non' => false,
                    'Oui' => true,
                ],
            ])
            ->add('category', ModelType::class, [
                'label' => 'Categorie',
                'class' => Category::class,
                'property' => 'name',
            ])
            ->add('articleTags', ModelAutocompleteType::class, [
                'class' => ArticleTag::class,
                'property' => 'name',
                'multiple' => true
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('content')
            ->add('status')
            ->add('publishDate')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', TextType::class, [
                'label' => 'Titre'
            ])
            ->add('category.name', TextType::class, [
                'label' => 'Categorie'
            ])
            ->add('status', null, [
                'label' => 'Publié'
            ])
            ->add('image', TextType::class, [
                'label' => 'Image'
            ])
            ->add('isMainArticle', null, [
                'label' => 'Article principale'
            ])
            ->add('publishDate', null, [
                'label' => 'Date publication'
            ])
            ->add('updateDate', null, [
                'label' => 'Date edition'
            ])
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => []
                ]
            ])
        ;
    }
}