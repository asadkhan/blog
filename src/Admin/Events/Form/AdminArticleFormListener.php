<?php


namespace App\Admin\Events\Form;

use App\Services\FileUploader;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\File\File;

class AdminArticleFormListener implements EventSubscriberInterface
{
    private $fileUploader;

    public function __construct(FileUploader $fileUploader)
    {
        $this->fileUploader = $fileUploader;
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'onPreSetData',
            FormEvents::PRE_SUBMIT   => 'onPreSubmit',
            FormEvents::POST_SUBMIT => 'onPostSubmit'
        ];
    }

    public function onPreSetData(FormEvent $event)
    {
        $article = $event->getData();
        $form = $event->getForm();
    }

    public function onPreSubmit(FormEvent $event)
    {
        $article = $event->getData();
        $form = $event->getForm();


    }

    public function onPostSubmit(FormEvent $event)
    {
        $article = $event->getData();
        $form = $event->getForm();
    }
}