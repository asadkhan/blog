<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query\ResultSetMapping;
use PDO;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function getArticles(?int $limit = null, ?array $notIn = null, ?bool $returnQuery = false){
        $qb = $this->createQueryBuilder('a');
        $qb->leftJoin('a.category', 'c')
            ->where('a.status = :status')
            ->setParameter('status', true)
            ->orderBy('a.id', 'DESC');

        if($notIn){
            $qb->andWhere('a NOT IN (:articles)')
                ->setParameter('articles', $notIn);
        }

        if($limit){
            $qb->setMaxResults($limit);
        }

        if($returnQuery){
            return $qb->getQuery();
        }else{
            return $qb->getQuery()->getResult();
        }
    }

    public function getArticlesByCategory($categoryId){
        $qb = $this->createQueryBuilder('a');
        $qb->leftJoin('a.category', 'c')
            ->where('a.status = :status')
            ->setParameter('status', true)
            ->andWhere('c = :categoryId')
            ->setParameter('categoryId', $categoryId)
            ->orderBy('a.id', 'DESC');

        return $qb->getQuery();
    }


    public function getMainArticle(){
        $article = $this->findBy(
            array(
                'isMainArticle' => true,
                'status' => true
            )
        );

        return $article;
    }


    public function getLatestCommentedArticles(?int $limit = null){
        $sql =<<<SQL
                select article_content.*
                from
                (
                    select a.*, c.publish_date comment_publish_date, c.content commentaire
                    from article a
                    inner join comment c on c.article_id = a.id
                    group by a.id, c.id
                    order by c.publish_date DESC
                ) article_content 
                group by article_content.id
                order by article_content.comment_publish_date DESC LIMIT {$limit}
SQL;
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        return $result;
    }

    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
