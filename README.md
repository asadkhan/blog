# Blog Project

## Before to run your project
* Stop all services runing on your machine
```
sudo service mysql stop
sudo service apache2 stop
```

## Configuration of project
* Configure your project under .env file
```
APP_USER=your_machine_user
APP_GROUP=your_machine_group
PROJECT_DIR=/var/www/your_project
```

* Make sure your docker/apache/app.conf is correct.
If you have changed the PROJECT_DIR, adapt your app.conf file

## Installation of project
* Installation of project
```
docker-compose build
docker-compose up -d
```

## Installation of dependencies
* Installation of php dependencies
```
docker-compose exec -u your_user php bash
composer install
```

* Installation of assets
```
npm install
yarn install
yarn encore dev
```

## Import database
* Generate project tables
```
docker-compose exec -u your_user php bash
bin/console doctrine:migrations:diff
bin/console doctrine:migrations:execute --up migration_number
```

* Or dump existing database
```
docker-compose exec mysql bash
mysql -u your_user -p database_name < blog.sql
```

## Create an admin user
```
php bin/console fos:user:create your_user your_email your_password
php app/console fos:user:activate your_user
php bin/console fos:user:promote your_user ROLE_ADMIN
```
